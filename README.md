# LeBonCoin

## Test technique LeBonCoin 

Réalisation d'une application Android native en Kotlin suivant une architecture MVVM.
J'ai suivi les bonnes pratiques de developpement proposé par https://developer.android.com/jetpack/guide.
Pour cela j'ai developpé l'application en MVVM, avec Room, Retrofit, ou encore les LiveDatas. 

## Les principales libraires  

### Room :
Permet de sauvegarder des données en local dans l'app-database.

### Retrofit :
Permet de récupérer des informations via des API HTTP
J'ai utilisé Retrofit afin de gagner du temps sur le developpement de l'application et en utilisant une librairies qui permet de faire du type safe http.

### Livedata :
Permet d'observer de façon cycle-aware des datas. Les liveDatas font également parti des bonnes pratiques de developpement.

### Coroutines :
Permet de gérer le code de façon asynchrone 

### Glide :
Permet d'afficher des images. 
Glide est plus rapide que Picasso ou Coil pour charger des images pour la première fois ou depuis le cache.
(https://proandroiddev.com/coil-vs-picasso-vs-glide-get-ready-go-774add8cfd40)

### Mockito : 
Permet de faciliter la rédaction des tests unitaires 

### Espresso :
 Permet de faire des tests fonctionnels
