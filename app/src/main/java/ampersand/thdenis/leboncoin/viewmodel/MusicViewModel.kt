package ampersand.thdenis.leboncoin.viewmodel

import ampersand.thdenis.leboncoin.model.Music
import ampersand.thdenis.leboncoin.repository.MusicRepository
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData

class MusicViewModel(private val musicRepository: MusicRepository) : ViewModel() {

    var allMusic: LiveData<List<Music>> = musicRepository.allMusics.asLiveData()

    var allMusicFromAlbum: MutableLiveData<List<Music>> =
        musicRepository.allMusicsFromAlbum.asLiveData() as MutableLiveData<List<Music>>

    var currentMusic: MutableLiveData<Music> =
        musicRepository.currentMusic.asLiveData() as MutableLiveData<Music>

    fun getMusics() = musicRepository.getMusics(allMusic as MutableLiveData<List<Music>>)

    fun saveAllMusics() = musicRepository.saveAllMusics(allMusic as MutableLiveData<List<Music>>)

    fun getMusic(id: Int) {
        musicRepository.getMusic(id)
        currentMusic = musicRepository.currentMusic.asLiveData() as MutableLiveData<Music>
    }

    fun getMusicsFromAlbum(musicId: Int) {
        musicRepository.getMusicsFromAlbum(musicId)
        allMusicFromAlbum =
            musicRepository.allMusicsFromAlbum.asLiveData() as MutableLiveData<List<Music>>
    }

}