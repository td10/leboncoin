package ampersand.thdenis.leboncoin.api

import ampersand.thdenis.leboncoin.model.Music
import retrofit2.Call
import retrofit2.http.GET

interface Api {

    @GET("img/shared/technical-test.json")
    fun getMusics(): Call<List<Music>>
}