package ampersand.thdenis.leboncoin.dao

import ampersand.thdenis.leboncoin.model.Music
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface MusicDao {

    @Query("SELECT * FROM music_table")
    fun getAllMusicsFlow(): Flow<List<Music>>

    @Query("SELECT * FROM music_table")
    fun getAllMusics(): List<Music>

    @Query("SELECT * FROM music_table as T1 INNER JOIN music_table as T2 WHERE T1.id = :musicId AND T1.albumId = T2.albumId")
    fun getAllMusicsFromCurrentAlbumFlow(musicId: Int): Flow<List<Music>>

    @Query("SELECT * FROM music_table as T1 INNER JOIN music_table as T2 WHERE T1.id = :musicId AND T1.albumId = T2.albumId")
    fun getAllMusicsFromCurrentAlbum(musicId: Int): List<Music>

    @Query("SELECT * FROM music_table WHERE id = 1")
    fun getFirstMusicFlow(): Flow<Music>

    @Query("SELECT * FROM music_table WHERE id = 1")
    fun getFirstMusic(): Music

    @Query("SELECT * FROM music_table WHERE id = :id")
    fun getMusicFlow(id: Int): Flow<Music>

    @Query("SELECT * FROM music_table WHERE id = :id")
    fun getMusic(id: Int): Music

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(musics: List<Music>?)

}