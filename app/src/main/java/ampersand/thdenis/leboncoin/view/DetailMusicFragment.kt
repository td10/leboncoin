package ampersand.thdenis.leboncoin.view

import ampersand.thdenis.leboncoin.MusicApplication
import ampersand.thdenis.leboncoin.adapter.MusicInAlbumAdapter
import ampersand.thdenis.leboncoin.databinding.FragmentMusicDetailsBinding
import ampersand.thdenis.leboncoin.factory.MusicViewModelFactory
import ampersand.thdenis.leboncoin.viewmodel.MusicViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class DetailMusicFragment : Fragment() {

    private var _binding: FragmentMusicDetailsBinding? = null
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView
    private lateinit var musicAdapter: MusicInAlbumAdapter

    private val musicViewModel: MusicViewModel by viewModels {
        MusicViewModelFactory((requireActivity().application as MusicApplication).repository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMusicDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = arguments?.getInt("id")
        id?.let {
            musicViewModel.getMusic(id)
            musicViewModel.getMusicsFromAlbum(id)
        }
        observeResponseData()

        musicAdapter = MusicInAlbumAdapter(listOf())
        recyclerView = binding.rvMusics
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = musicAdapter
    }


    private fun observeResponseData() {
        musicViewModel.currentMusic.observe(viewLifecycleOwner, Observer { music ->
            music?.let {
                requireActivity().title = music.title
                binding.tvCurrentTitle.text = music.title

                val url = GlideUrl(
                    music.url, LazyHeaders.Builder()
                        .addHeader("User-Agent", "user-agent")
                        .build()
                )

                Glide.with(requireContext())
                    .load(url)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(binding.ivUrl)
            }
            musicViewModel.getMusicsFromAlbum(music.id)
        })

        musicViewModel.allMusicFromAlbum.observe(viewLifecycleOwner, Observer { musics ->
            musics?.let {
                musicAdapter.setMusics(musics)
            }
        })
    }
}