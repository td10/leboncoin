package ampersand.thdenis.leboncoin.view

import ampersand.thdenis.leboncoin.MusicApplication
import ampersand.thdenis.leboncoin.R
import ampersand.thdenis.leboncoin.adapter.MusicAdapter
import ampersand.thdenis.leboncoin.databinding.FragmentAllMusicsBinding
import ampersand.thdenis.leboncoin.factory.MusicViewModelFactory
import ampersand.thdenis.leboncoin.viewmodel.MusicViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class AllMusicsFragment : Fragment(), View.OnClickListener {

    private var _binding: FragmentAllMusicsBinding? = null
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView
    private lateinit var musicAdapter: MusicAdapter

    private val musicViewModel: MusicViewModel by viewModels {
        MusicViewModelFactory((requireActivity().application as MusicApplication).repository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAllMusicsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().title = getString(R.string.all_music_title)

        musicViewModel.getMusics()
        observeResponseData()

        musicAdapter = MusicAdapter(listOf())
        recyclerView = binding.rvMusics
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = musicAdapter

        binding.fabGoTop.setOnClickListener(this)
    }

    private fun observeResponseData() {
        musicViewModel.allMusic.observe(viewLifecycleOwner, Observer { musics ->
            if (musics.isNotEmpty()) {
                binding.rvMusics.visibility = VISIBLE
                binding.tvNoInternet.visibility = GONE
                musicAdapter.setMusics(musics)
            } else {
                binding.rvMusics.visibility = GONE
                binding.tvNoInternet.visibility = VISIBLE
            }
        })
    }

    override fun onPause() {
        GlobalScope.launch {
            musicViewModel.saveAllMusics()
        }
        super.onPause()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.fab_go_top -> {
                recyclerView.smoothScrollToPosition(0)
            }
        }
    }
}