package ampersand.thdenis.leboncoin.helper

import ampersand.thdenis.leboncoin.model.Music
import org.junit.Before

open class HelperTest {
    val album1 = arrayListOf(
        createMusic(1), createMusic(2), createMusic(3)
    )
    val album2 = arrayListOf(
        createMusic(4, 2), createMusic(5, 2), createMusic(6, 2)
    )
    val musics: ArrayList<Music> = arrayListOf()

    @Before
    open fun setUp() {
        musics.addAll(album1)
        musics.addAll(album2)
    }

    fun createMusic(id: Int): Music =
        Music(
            id = id,
            albumId = 1,
            title = "title",
            url = "url",
            thumbnailUrl = "thumbnailUrl"
        )

    fun createMusic(id: Int, albumId: Int): Music =
        Music(
            id = id,
            albumId = albumId,
            title = "title",
            url = "url",
            thumbnailUrl = "thumbnailUrl"
        )
}