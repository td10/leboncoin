package ampersand.thdenis.leboncoin

import ampersand.thdenis.leboncoin.dao.MusicDao
import ampersand.thdenis.leboncoin.helper.CoroutineTestRule
import ampersand.thdenis.leboncoin.helper.HelperTest
import ampersand.thdenis.leboncoin.helper.LifeCycleTestOwner
import ampersand.thdenis.leboncoin.model.Music
import ampersand.thdenis.leboncoin.repository.MusicRepository
import ampersand.thdenis.leboncoin.viewmodel.MusicViewModel
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.CountDownLatch

@RunWith(MockitoJUnitRunner::class)
class MusicViewModelTest : HelperTest() {
    @get:Rule
    val instantTaskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineTestRule =
        CoroutineTestRule()

    @Mock
    private lateinit var mockDao: MusicDao

    @Mock
    private lateinit var mockRepo: MusicRepository


    private val stateObserver: Observer<Lifecycle.State> = mock()
    private lateinit var lifeCycleTestOwner: LifeCycleTestOwner
    private lateinit var viewModel: MusicViewModel
    private lateinit var latch: CountDownLatch
    private var isSucceed = false

    @Before
    override fun setUp() {
        super.setUp()
        lifeCycleTestOwner =
            LifeCycleTestOwner()
        lifeCycleTestOwner.onCreate()
        isSucceed = false


        latch = CountDownLatch(1)

        `when`(mockRepo.allMusics).thenReturn(
            flow { musics })
        `when`(mockRepo.allMusicsFromAlbum).thenReturn(
            flow { musics })
        `when`(mockRepo.currentMusic).thenReturn(
            flow { musics[0] })

        viewModel = MusicViewModel(mockRepo)


        `when`(mockRepo.getMusics(viewModel.allMusic as MutableLiveData<List<Music>>)).then {
            (it.arguments[0] as MutableLiveData<List<Music>>).postValue(musics)
        }

        doAnswer {
            viewModel.currentMusic.postValue(musics[it.arguments[0] as Int])
            return@doAnswer mockRepo.currentMusic
        }.`when`(mockRepo).getMusic(ArgumentMatchers.anyInt())

        doAnswer {
            when (it.arguments[0] as Int) {
                1, 2, 3 -> (viewModel.allMusicFromAlbum).postValue(album1)
                4, 5, 6 -> (viewModel.allMusicFromAlbum).postValue(album2)
                else -> {
                    println("Value not exepected")
                    latch.countDown()
                }
            }
        }.`when`(mockRepo).getMusicsFromAlbum(ArgumentMatchers.anyInt())
    }

    @After
    fun tearDown() {
        lifeCycleTestOwner.onDestroy()
    }

    @Test
    fun test_getMusics() {

        viewModel.allMusic.observeForever {
            isSucceed = musics == viewModel.allMusic.value
            latch.countDown()
        }

        GlobalScope.launch {
            viewModel.getMusics()
        }
        try {
            latch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        Assert.assertTrue(isSucceed)
    }

    @Test
    fun test_getMusic() {

        viewModel.currentMusic.observeForever {
            isSucceed = musics[0] == viewModel.currentMusic.value
            latch.countDown()
        }

        GlobalScope.launch {
            viewModel.getMusic(0)
        }
        try {
            latch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        Assert.assertTrue(isSucceed)
    }

    @Test
    fun test_getMusicsFromAlbum() {

        viewModel.allMusicFromAlbum.observeForever {
            isSucceed = album1 == viewModel.allMusicFromAlbum.value
            latch.countDown()
        }

        GlobalScope.launch {
            viewModel.getMusicsFromAlbum(1)
        }
        try {
            latch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        Assert.assertTrue(isSucceed)
    }
}