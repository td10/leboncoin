package ampersand.thdenis.leboncoin

import ampersand.thdenis.leboncoin.api.RetrofitInstance
import ampersand.thdenis.leboncoin.model.Music
import junit.framework.Assert.assertTrue
import org.junit.Test
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.CountDownLatch

class ApiTest{

    @Test
    fun testApiResponse() {
        var isSucceed = false
        val latch = CountDownLatch(1)
        val call: Call<List<Music>> = RetrofitInstance.api.getMusics()
        call.enqueue(object : Callback<List<Music>> {
            override fun onResponse(call: Call<List<Music>>, response: Response<List<Music>>) {
                try {
                    isSucceed = response.code() == 200
                } finally {
                    latch.countDown()
                }
            }

            override fun onFailure(call: Call<List<Music>>, t: Throwable) {
                latch.countDown()
            }
        })
        try {
            latch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        assertTrue(isSucceed)
    }
}